#include <iostream>
#include <list>

using namespace std;
int len(string);
bool isBigger(string,string);
bool isSmaller(string,string);
template <class T> class myVec
{
public:
    myVec()
    {
        vSize = 0;
        vCapacity = 10;
        vArr = new T[10];
    }
    T& operator[ ](unsigned int i)
    {
        return vArr[i];
    }
    T& operator=(const T& other)
    {
        vSize = other.vSize;
        vCapacity = other.vCapacity;
        vArr = other.vArr;
        return *this;
    }
    void pushBack(const T& item)
    {
        if(vSize==vCapacity)
        {
            reserve(vCapacity*2 ,true);
        }
        vArr[vSize] = item;
        vSize++;
    }
    void remove(int index)
    {
        for(int i=index;i<vSize-1;i++)
        {
             vArr[i]=vArr[i+1];
        }
        vSize--;
    }
    void remove(const T& item)
    {
        int index = -1;
        for(int i=0;i<vSize;i++)
        {
            if(vArr[i]==item)
            {
                index = i;
                break;
            }
        }
        if(index!=-1)
        {
            remove(index);
        }
    }
    bool contains(const T& item)
    {
        for(int i=0;i<vSize;i++)
        {
            if(vArr[i] == item)
            {
                return true;
            }
        }
        return false;
    }
    int size()
    {
        return vSize;
    }
    void reserve(int newSize,bool copy)
    {
        T* newArr = new T[newSize];
        if(copy)
        {
            for(int i=0;i<vSize;i++)
            {
                newArr[i] = vArr[i];
            }
        }
        if(vArr != NULL)
        {
            delete[] vArr;
        }
        vArr = newArr;
        vCapacity = newSize;
    }

private:
    int vSize;
    int vCapacity;
    T* vArr;
};
class Gene
{
public:
    Gene()
    {
        isNameSet= false;
        visited = false;
    }
    int x1,x2;
    bool isNameSet;
    string mainName;
    myVec<string> names;
    bool visited;
    list<Gene> regulatedGenes;
    //myVec<Gene> regulatedGenes;
    bool operator== (const Gene& second)
    {
        if(x1==second.x1 && x2==second.x2)
        {
            return true;
        }
        return false;
    }
    void setName()
    {
        int random = rand()%(names.size());
        mainName = names[random];
        isNameSet = true;
    }
    void printGene()
    {
        cout<<x1<<" "<<x2<<" ";
        for(int i=0;i<names.size();i++)
        {
            cout<<names[i]<<" ";
        }
        cout<<endl;
    }
    void printAdjacents()
    {
        for(Gene gene : regulatedGenes)
        {
            if(!gene.isNameSet)
            {
                gene.setName();
            }
            cout<<mainName<<" ==> "<<gene.mainName<<endl;
        }
    }
    void bfs(Gene gene , int k)
    {
        int counter=1;
        gene.visited = true;
        list<Gene> queue;
        queue.push_back(gene);
        list<Gene> state;
        state.push_back(gene);
        list<Gene>::iterator i;
        while (!queue.empty())
        {
            gene = queue.front();
            state.push_back(gene);
            queue.pop_front();
            for(i = gene.regulatedGenes.begin();i!=gene.regulatedGenes.end();i++)
            {
                if(!i->visited)
                {
                    i->visited = true;
                    queue.push_back(*i);
                    counter++;
                    //state.push_back(*i);
                }
                if(i->mainName == gene.mainName && counter<=k)
                {
                    for(Gene gene1 : state)
                    {
                        cout<<gene1.mainName<<" , ";
                    }
                    cout<<endl;
                }
            }
        }
    }
};
bool containsInList(list<Gene> lst,Gene gne)
{
    for(Gene gene : lst)
    {
        if(gne == gene)
        {
            return true;
        }
    }
    return false;
}
struct Node
{
    string key;
    Node* left;
    Node* right;
    Gene* value;
};
class splayTree
{
public:
    splayTree()
    {
        ///Nothing
    }
    Node* RR_Rotate(Node* key2)
    {
        Node* key1 = key2->left;
        key2->left = key1->right;
        key1->right = key2;
        return key1;
    }
    Node* LL_Rotate(Node* key2)
    {
        Node* key1 = key2->right;
        key2->right = key1->left;
        key1->left = key2;
        return key1;
    }
    Node* splay(string key,Node* root)
    {
        if(!root)
        {
            return NULL;
        }
        Node header;
        header.left = header.right = NULL;
        Node* leftTreeMax = &header;
        Node* rightTreeMin = &header;
        while (true)
        {
            if (isSmaller(key,root->key)/*key < root->key*/)
            {
                if (!root->left)
                    break;
                if (isSmaller(key,root->left->key)/*key < root->left->key*/)
                {
                    root = RR_Rotate(root);
                    if (!root->left)
                        break;
                }
                rightTreeMin->left = root;
                rightTreeMin = rightTreeMin->left;
                root = root->left;
                rightTreeMin->left = NULL;
            }
            else if (isBigger(key,root->key)/*key > root->key*/)
            {
                if (!root->right)
                    break;
                if (isBigger(key,root->right->key)/*key > root->right->key*/)
                {
                    root = LL_Rotate(root);
                    if (!root->right)
                        break;
                }
                leftTreeMax->right = root;
                leftTreeMax = leftTreeMax->right;
                root = root->right;
                leftTreeMax->right = NULL;
            }
            else
                break;
        }
        leftTreeMax->right = root->left;
        rightTreeMin->left = root->right;
        root->right = header.right;
        root->right = header.right;
        return root;
    }
    Node* newNode(string key,Gene* val)
    {
        Node* node = new Node;
        if (!node)
        {
            fprintf(stderr, "Out of memory!\n");
            exit(1);
        }
        node->key = key;
        node->value = val;
        node->right = node->right = NULL;
        return node;
    }
    Node* insert(string key,Gene* val, Node* root)
    {
        static Node* node = NULL;
        if (!node)
            node = newNode(key,val);
        else
        {
            node->key=key;
            node->value = val;
        }
        if (!root)
        {
            root = node;
            node = NULL;
            return root;
        }
        root = splay(key, root);
        if (isSmaller(key,root->key)/*key < root->key*/)
        {
            node->left = root->left;
            node->right = root;
            root->left = NULL;
            root = node;
        }
        else if (isBigger(key,root->key)/*key > root->key*/)
        {
            node->right = root->right;
            node->left = root;
            root->right = NULL;
            root = node;
        }
        else
            return root;
        node = NULL;
        return root;
    }
    Node* Delete(string key, Node* root)
    {
        Node* temp;
        if (!root)
            return NULL;
        root = splay(key, root);
        if (key != root->key)
            return root;
        else
        {
            if (!root->left)
            {
                temp = root;
                root = root->left;
            }
            else
            {
                temp = root;
                root = splay(key, root->left);
                root->right = temp->right;
            }
            free(temp);
            return root;
        }
    }
    Node* search(string key, Node* root)
    {
        return splay(key, root);
    }
    Node* searchWithoutSplay(string key,Node* root)
    {
        Node* temp = root;
        if(root==NULL)
        {
            return NULL;
        } else
        {
            while (true)
            {
                if(isSmaller(key,temp->key))
                {
                    if(temp->left==NULL)
                    {
                        return NULL;
                    }
                    temp = temp->left;
                } else if(isBigger(key,temp->key))
                {
                    if(temp->right==NULL)
                    {
                        return NULL;
                    }
                    temp = temp->right;
                } else{
                    return temp;
                }
            }
        }
    }
};
int main() {
    bool allSet = false;
    myVec<Gene> geneList;
    splayTree tree;
    Node* root;
    root = NULL;
    cout<<"====================================================="<<endl;
    cout<<"Please Enter the Commands with its argument : (AddGene,AddGeneAlias,RemoveAlias,Regulates,DonateRegulate,PrintFeedBackLoops,PrintGeneInfo,Quit)"<<endl;
    while (true)
    {
        string command;
        cin>>command;
        if(command=="AddGene")
        {
            int x1,x2;
            string name;
            cin>>name>>x1>>x2;
            if(root==NULL)
            {
                Gene gene;
                gene.x1=x1;
                gene.x2=x2;
                gene.names.pushBack(name);
                geneList.pushBack(gene);
                root = tree.insert(name,&geneList[0],root);
                cout<<"Command Done !"<<endl;
            }
            else
            {
                root = tree.search(name,root);
                if(root->key!=name)
                {
                    bool flag = false;
                    for(int i=0;i<geneList.size();i++)
                    {
                        if(geneList[i].x1==x1 && geneList[i].x2==x2 && !geneList[i].names.contains(name))
                        {
                            geneList[i].names.pushBack(name);
                            root = tree.insert(name,&geneList[i],root);
                            cout<<"Command Done !"<<endl;
                            flag= true;
                        }
                    }
                    if(!flag)
                    {
                        Gene gene;
                        gene.x1=x1;
                        gene.x2=x2;
                        gene.names.pushBack(name);
                        geneList.pushBack(gene);
                        root = tree.insert(name,&geneList[geneList.size()-1],root);
                        cout<<"Command Done !"<<endl;
                    }
                }
                else
                {
                    cout<<"ERORR , The Name is Already Used "<<endl;
                }
            }
        } else if(command == "AddGeneAlias")
        {
            string name1,name2;
            cin>>name1>>name2;
            if(root!=NULL)
            {
                root = tree.search(name2,root);
                if(root->key==name2)
                {
                    cout<<"ERORR , "<<name2<<" is Already Used as a Gene's name"<<endl;
                } else
                {
                    root = tree.search(name1,root);
                    if(root->key!=name1)
                    {
                        cout<<"ERORR , "<<name1<<" doesn't Exist !"<<endl;
                    } else
                    {
                        Gene* temp = root->value;
                        root->value->names.pushBack(name2);
                        root = tree.insert(name2,temp,root);
                        cout<<"Command Done !"<<endl;
                    }

                }
            } else
            {
                cout<<"ERORR , Tree is Empty !"<<endl;
            }
        }else if(command == "RemoveAlias")
        {
            string name;
            cin>>name;
            if(root!=NULL)
            {
                root = tree.search(name,root);
                if(root->key!=name)
                {
                    cout<<"ERORR ! "<<name<<" doesn't Exist "<<endl;
                } else
                {
                    root->value->names.remove(name);
                    if(root->value->names.size()==0)
                    {
                        geneList.remove(*root->value);
                    }
                    root = tree.Delete(name,root);
                    cout<<"Command Done !"<<endl;
                }
            }
            else
            {
                cout<<"ERORR , Tree is Empty !"<<endl;
            }
        }else if(command == "Regulates")
        {
            string name1,name2;
            cin>>name1>>name2;
            
        }
        else if(command == "DonateRegulate")
        {
            string name1,name2;
            cin>>name1>>name2;
            if(root!=NULL)
            {
                root = tree.search(name2,root);
                if(root->key == name2)
                {
                    Gene* temp = root->value;
                    root = tree.search(name1,root);
                    if(root->key==name1)
                    {
                        if(containsInList(root->value->regulatedGenes,*temp))
                        {
                            root->value->regulatedGenes.remove(*temp);
                            cout<<"Command Done !"<<endl;
                        } else
                        {
                            cout<<"ERORR ! "<<name1<<" doesn't Regulate "<<name2<<endl;
                        }
                    } else
                    {
                        cout<<"ERORR ! "<<name1<<" doesn'nt exist !"<<endl;
                    }

                }
            } else
            {
                cout<<"ERORR , Tree is Empty !"<<endl;
            }
        }
        else if(command == "PrintGeneInfo")
        {
            string name;
            cin>>name;
            if(root!=NULL)
            {
                root = tree.search(name,root);
                if(root->key==name)
                {
                    root->value->printGene();
                } else
                {
                    cout<<"ERORR ! "<<name<<" doesn't Exist"<<endl;
                }
            }else
            {
                cout<<"ERORR , Tree is Empty !"<<endl;
            }
        }
        else if(command == "PrintGraph")
        {
            for(int i=0;i<geneList.size();i++)
            {
                if(!geneList[i].isNameSet)
                {
                    geneList[i].setName();
                }
                geneList[i].printAdjacents();
            }
        }
        else if(command == "PrintFeedBackLoops")
        {
            string name;
            int k;
            cin>>name>>k;
        }
        else if(command == "Quit")
        {
            break;
        }else
        {
            cout<<"Invalid Command . Please Enter The Correct Command "<<endl;
        }
        cout<<"Please Enter the Commands with its argument : (AddGene,AddGeneAlias,RemoveAlias,Regulates,DonateRegulate,PrintGeneInfo,Quit)"<<endl;
    }
    return 0;
}
int len(string st)
{
    int counter=0;
    while (st[counter]!='\0')
    {
        counter++;
    }
    return counter;
}
bool isBigger(string first,string second)
{
    for(int i=0;i<len(first) && i<len(second);i++)
    {
        if(first[i]>second[i])
            return true;
        else if(first[i]<second[i])
            return false;
    }
    if(len(first)>len(second))
        return true;
    else
        return false;
}
bool isSmaller(string first,string second)
{
    for(int i=0;i<len(first) && i<len(second);i++)
    {
        if(first[i]<second[i])
            return true;
        else if(first[i]>second[i])
            return false;
    }
    if(len(first)<len(second))
        return true;
    else
        return false;
}